---
title: "Exercici de la Sessió 9"
subtitle: "Curs R (OMD)"
author: "Xavier de Pedro"
date: "7 de Maig de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css 
tags:
- Exercici
- Curs
- R
- Formació
always_allow_html: yes
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Tasques

1. A l'interior de l'arxiu descarregat de CartoBCN (**CartoBCN-20190419_DIVADM.zip**) hi havia arxius SHP de Districtes i de barris, entre altres. Representa el mapa de barris de Barcelona amb `ggplot2::geom_sf()`, de forma anàloga a com s'ha mostrat als apunts per als Districtes. 
1. A partir del dataset b (SAIER), fes un mapa del mon on es mostrin en diagrames de pastisos la proporció d'Homes i Dones ateses al SAIER per a cada país d'origen (`ProcedenciaAngles`, i no pas per a la nacionalitat del seu passaport `NacionalitatAngles`, com s'havia fet fins ara).

# Respondre a aquestes preguntes

```{r}
# Dur les respostes a l'inici de la segûent sessió
```

1. Quina diferència hi ha entre els mapes del món fet amb el paquet `rworldmap`, i els fets amb `ggiraph`?

2. Quines diferències hi ha entre els mapes fets a partir de `tmap` i els fets a partir de `plotly`?

3. Com podem incloure en un arxiu **pdf** un mapa interactiu fet amb `Leaflet`? 


