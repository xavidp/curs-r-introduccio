---
title: "Exemple B: 'Urgències al SAIER'"
subtitle: "Sessió 06 - Curs R (OMD)"
author: "Xavier de Pedro"
date: "11 d'Abril de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  html_document:
    df_print: paged
    toc: yes
    toc_float: yes
    number_sections: yes
    code_folding: show
    keep_md: false
    includes:
      in_header: exercici_ajbcn_capcalera.html
      before_body: exercici_ajbcn_abans.html
      after_body: exercici_ajbcn_despres.html
abstract: |
  Informació del Curs: 
  https://omd-gid.imi.bcn/CursR
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
if (!require("pacman")) install.packages("pacman"); require(pacman)
```
# Objectiu

* analitzar una llista de registres amb informació numèrica, categòrica, textual, amb dates i ubicació geogràfica que serà emprada per geoposicionar registres i mostra sobre mapes.
* posar en pràctica el coneixement après fins a la data en cada moment
* objectiu final: informe en MS Word (amb plantilla de l'Ajuntament de Barcelona) amb resums en taules estàtiques i dinàmiques, gràfics estàtics i dinàmics, i relació de paquets i versions de programari emprats per produir-lo.

Agraïments:

* Gloria Rendon i Ramon Sanahuja. Direcció d'Atenció i Acollida d'Immigrants. Drets de Ciutadania, Cultura, Participació i Transparència. Ajuntament de Barcelona.
* Equip del CUESB - Centre d'Urgències i Emergències Socials de Barcelona.

# Carreguem paquets

```{r}
if (!require("devtools")) install.packages("devtools"); require(devtools)
if (!require("pacman")) install_version("pacman", version = "0.4.1"); require(pacman)
p_load(tidyverse, janitor)
```
```{r echo=F}
# I preprocessem les dades originals una vegada
#
# if (F) {
#   # Genero el csv des de l'Excel original
#   arxiu <- read_csv("../dades/b/saier_2017_old.csv")
#   arxiu <- arxiu %>%
#     mutate(Id = rownames(arxiu)) %>%
#     transmute(Id,
#               CodiCas=CodiMaria,
#               Nom_Cognom=str_c(Nom, ",", Cognoms),
#               Edat,
#               GrupEdat,
#               DataDerivacioSAIER,
#               AnyDerivText,
#               MesDerivacioSAIER,
#               AlertesOrientacionsDe,
#               ComunicacioDerivacio,
#               Nacionalitat,
#               Continent,
#               NacionalitatAngles,
#               LlocProcedencia,
#               ContinentProced,
#               ProcedenciaAngles,
#               DataArribadaBCN,
#               MesArribadaBCNDesDe2008,
#               TrimArribadaBCNDesDe2008,
#               HomeDona,
#               NucliIndividu,
#               NMembresNucli,
#               SituacioAdministrativa,
#               PaisTramitSituacioAdministrativa,
#               Perfil,
#               DetallObservacions
#     )
#   write_csv(arxiu, path="../dades/b/saier_2017.csv")
# }
```

# Llegim dades

Llegeix l'arxiu de dades `saier_2017.csv` i assigna el seu contingut a la variable `arxiu`
```{r}
arxiu <- read_csv("../dades/b/saier_2017.csv")
```

---

Llegeix el mateix arxiu de dades `saier_2017.csv` amb la instrucció equivalent del R base `read.csv`, assignant el seu contingut a la variable `arxiu2`
```{r}
# Escriu el codi necessari en aquest chunk d'R:
arxiu2<- read.csv("../dades/b/saier_2017.csv")
```

---

Compara si són iguals amb la instrucció `setdiff()` a la consola d'R directament (i no pas dins d'un chunk d'R a un arxiu .Rmd), que ens mostraria les diferències si existeixen, i en cas d'haver-hi warnings, mostraria una línia de resum de l'estil:

    > There were X warnings (use warnings() to see them)

```{r}
# Escriu el codi necessari en aquest chunk d'R:
setdiff(arxiu, arxiu2)
```

---

Fíxa't en el missatge de la consola d'R en color vermell:

    > There were 24 warnings (use warnings() to see them)

Escriu `warnings()` a la comsola d'R per veure els missatges.


```{r}
# Escriu el codi necessari en aquest chunk d'R:
warnings()
```

---

Prova ara de tornar a llegir el mateix arxiu de dades amb `read.csv()` però ara especificant el paràmetre stringsAsFactors com a `FALSE`. 

```{r}
# Escriu el codi necessari en aquest chunk d'R:
arxiu2<- read.csv("../dades/b/saier_2017.csv", stringsAsFactors=FALSE)
```

I compara de nou si apareix el mateix warning en executar la funció `setdiff()` entre els data frames `arxiu` i `arxiu2`


```{r}
# Escriu el codi necessari en aquest chunk d'R:
setdiff(arxiu, arxiu2)
```

## Fem una ullada als registres

Fes que ens mostri els primers 6 registres de l'objecte `arxiu` amb la instrucció `head()`

```{r}
# Escriu el codi necessari en aquest chunk d'R:
head(arxiu)
```


# Neteja Inicial

```{r}
dades <- arxiu %>%
  separate(Nom_Cognom, into = c("Nom", "Cognoms")) %>% 
  separate(`DataDerivacioSAIER`, into = c("dds.dia", "dds.mes", "dds.any"), convert = TRUE) %>%
  select(-AnyDerivText,
         -DetallObservacions)
head(dades)
```

Per obtenir `%>%` podeu prémer: `[Ctrl] + [Shift] + m`

# Duplicats?

## Quants registres hi ha al data frame de dades?

```{r}
nrow(dades)
```


## Casos diferents?

```{r}
dades %>%
  distinct(CodiCas) %>%
  count()
```

## Quants Id diferents hi ha?

Pots fer-ho repetint el procediment anterior per a la columna `Id` del data frame `dades`.

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  distinct(Id) %>%
  count()
```

## De quines edats són?

Podem mirar-ho amb la funció `table()` sobre la columna concreta del data frame `dades`.

```{r}
# Escriu el codi necessari en aquest chunk d'R:
table(dades$Edat)
```

Ara bé, tenim un problema, ens mostra les edats com si fos text, i no pas números. I a més, l'ordenació de les edats també és errònia ("1 10 2 3 ...") per que no ho entèn com a número.

Comprovem de quina classe de vector es tracta, amb la funció `class()` sobre la coluomna **Edat**.

```{r}
# Escriu el codi necessari en aquest chunk d'R:
class(dades$Edat)
```

Podem provar de repetir la instrucció anterior però forçant a R a interpretar l'Edat com a números, amb la funció `as.numeric()`

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades$Edat <- as.numeric(dades$Edat)
table(dades$Edat)
```

Ara bé, si hi havia `nrow(dades)` registres, aquí tenim molts pocs valors (`sum(table(as.numeric(dades$Edat)))` exactament), i és probable que molts valors siguin o bé `NA` inicialment, o bé, convertits a `NA` en forçar a convertir a numeros els valors de la variable (que contenen casos de "`-`", "`mesos`", ...).

Refem la instrucció de mostrar la taula, però ara forcem a que ens mostri els NA si n'hi ha.

```{r}
# Escriu el codi necessari en aquest chunk d'R:
table(dades$Edat, useNA="ifany")
```

I ja els tenim, 839 NA en passar a numeros els valors.

I podem visualitzar-ho també amb la funció `tabyl()` del paquet `janitor`, que vam veure a la sessió passada que ve a ser com un `table()` millorat, i permet la sintaxi del `Tidyverse` amb pipes/canonades:

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>% 
  tabyl(Edat, show_na = TRUE,   show_missing_levels = TRUE) %>%
  adorn_totals(where = "row") %>%
  data.frame

```

Quan hi ha valors `NA`, `tabyl()` mostra també els percentatges "vàlids", és a dir, eliminant els valors que manquen del denominador del percentatge.

## Filtrem 

Quants menors de 18 anys trobem al data.frame `dades`?

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  filter(as.numeric(Edat) < 18) %>%
  count()

```

## Recompte de registres per continents i paísos

Quants registres hi ha per per país?

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  group_by(Nacionalitat) %>%
  arrange(Nacionalitat) %>%
  count()
```

Quants registres hi ha per continent i país?

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  group_by(Continent, Nacionalitat) %>%
  arrange(Nacionalitat) %>%
  count()
```

I quants registres hi ha per mes i Grup d'Edat?

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  group_by(dds.any, dds.mes, GrupEdat) %>%
#  arrange(MesDerivacioSAIER) %>%
  count()
```

O podem mostrar les dades de grup d'edat (Adult, Menor) en columnes, per a un informe.
He d'estendre (`spread()`) les dades d'una columna (`n`) en vàries, en funció dels factors d'una altra columna (`GrupEdat`)

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  group_by(dds.any, dds.mes, GrupEdat) %>%
  count() %>%
  spread(GrupEdat, n)
```

## Evolució mensual dels registres de majors d'edat

Quina instrucció de filtre hem d'aplicar, i a on?

```{r eval=FALSE}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  # filter(?)
  group_by(dds.any, dds.mes) %>%
  # filter(?)
  count()
  # filter(?)

```

## Orígens de les famílies pel nombre d'individus arribats de la família

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>% 
  count(Nacionalitat, NMembresNucli,
        wt = NULL,
        sort = FALSE) %>%
  filter(!is.na(Nacionalitat)
         ) %>%
  spread(NMembresNucli, n)
```


## Distribució d'adults i menors per països

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  group_by(Nacionalitat, GrupEdat) %>%
  arrange(Nacionalitat) %>%
  count() %>%
  spread(GrupEdat, n) %>%
  clean_names() %>%
  adorn_totals(where = c("row", "col"), fill = "-") %>%
  data.frame()
```


## Orígens dels menors

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  filter(GrupEdat == "Menor") %>%
  group_by(Nacionalitat) %>%
  count() %>%
  arrange(desc(n)) %>%
  adorn_totals(where = "row") %>%
  data.frame()
```

## Distribució per genere

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  group_by(Nacionalitat, HomeDona) %>%
  count() %>%
  spread(HomeDona, n) %>%
  clean_names() %>%
  adorn_totals(where = c("row", "col")) %>%
  data.frame()
```


## Situacions administratives

### Resum de situacions administratives

Aquí canviem els noms dels països pels continents, per provar de fer una taula resum més petita.
```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  group_by(Continent, SituacioAdministrativa) %>%
  count() %>%
  spread(SituacioAdministrativa, n) %>%
  clean_names() %>%
  adorn_totals(where = c("row", "col")) %>%
  data.frame()
```

Si calgués fer-ho per països, afegim (o reemplacem **Continent** per) la variable **Nacionalitat** al `group_by()` inicial.

### Evolució mensual (i totals) de les situacions administratives

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  group_by(dds.any, dds.mes, SituacioAdministrativa) %>%
  count() %>%
  spread(SituacioAdministrativa, n) %>%
  clean_names() %>%
  adorn_totals(where = c("row", "col")) %>%
  data.frame()
```

### Denegats a un altre país europeu

```{r}
tabyl(dades$SituacioAdministrativa)
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  filter(SituacioAdministrativa == "Denegat PI a altre país UE") %>%
  group_by(dds.any, dds.mes, Nacionalitat) %>%
  count() %>%
  spread(dds.mes, n)
```

## Perfils

### Resum de perfils

```{r}
# Escriu el codi necessari en aquest chunk d'R:
tabyl(dades$Perfil) %>%
  adorn_totals(where = "row") %>%
  data.frame
```

### Decretada la majoria d'edat

```{r}
# Escriu el codi necessari en aquest chunk d'R:
dades %>%
  filter(Perfil == "Decretada o recent majoria d'edat (Refugiat o Immigrant)") %>%
  group_by(dds.any, dds.mes, Nacionalitat) %>%
  count() %>%
  spread(dds.mes, n)
```

### Casos segons relació entre Situació Administrativa i Perfil

```{r}
# Escriu el codi necessari en aquest chunk d'R:
casos.sa.p <- dades %>%
  group_by(SituacioAdministrativa, Perfil) %>%
  count() %>%
  spread(Perfil, n)
casos.sa.p.llegenda.cols <- colnames(casos.sa.p)[-1]
colnames(casos.sa.p)[-1] <- paste0("V", 1:(ncol(casos.sa.p)-1))
casos.sa.p  %>%
  adorn_totals(where = c("row", "col")) %>%
  data.frame()
```

Llegenda de columnes: `r casos.sa.p.llegenda.cols`

```{r}
data.frame(casos.sa.p.llegenda.cols) %>%
  rowid_to_column() %>%
  mutate(Vn = str_c("V", rowid)) %>%
  select(Vn, 
         Casos = casos.sa.p.llegenda.cols)
```

## Origens de les derivacions al CUESB

### Resum dels origens de les derivacions

```{r}
# Escriu el codi necessari en aquest chunk d'R:
# 
tabyl(dades$AlertesOrientacionsDe) %>%
  adorn_totals(where = "row") %>%
  data.frame
```

### Evolució mensual (i totals) dels origens de les derivacions al CUESB

```{r}
# Escriu el codi necessari en aquest chunk d'R:
casos.origens <- dades %>%
  group_by(dds.any, dds.mes, AlertesOrientacionsDe) %>%
  count() %>%
  spread(dds.mes, n) 
```

# Afegim coordenades (Latitud i Longitud) per a mapes

En següents sessions, afegirem noves columnes amb les coordenades LatLong (Latitud i Longitud) per a facilitar la representació dels resultats sobre mapes de distribució geogràfica.

Aquestes noves columnes les afegirem amb la funció `inner_join()` del paquet `dplyr`, combinant les nostres dades en el dataframe `dades` (de les persones arribades a Barcelona) amb un dataframe de països i coordenades geogràfiques de les seves capitals, que tenim a la carpeta `dades/b/`, dins l'arxiu `paisos.csv`

```{r}
paisos <- read_csv("../dades/b/paisos.csv",
                   col_types = "cccddcccc", 
                   locale = locale("es", decimal_mark = ","))
paisos[,2:5]
```

# Fi

## Paquets i versions
Relació de paquets i versions de programari R emprats per produir-lo.

```{r}
cat("Sys.info() : \n")
cat("--------------------\n")
data.frame(Sys.info())
cat("\n\nsessionInfo() : \n")
cat("--------------------\n")
sessionInfo()
cat("--------------------\n")

```

