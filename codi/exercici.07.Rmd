---
title: "Exercici de la Sessió 7"
subtitle: "Curs R (OMD)"
author: "Xavier de Pedro"
date: "23 d'Abril de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css 
tags:
- Exercici
- Curs
- R
- Formació
always_allow_html: yes
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Tasques

1. Fes una **taula en format article** via `huxtable` a partir d'algun dataset del curs
1. Fes una **taula amb format condicional** via `condformat` a partir d'algun dataset del curs
1. Fes una **taula dinàmica interactiva** amb `rpivotTable` a partir d'algun dataset del curs
1. Fes una **taula dinàmica** amb `pivottabler` a partir d'algun dataset del curs


# Respondre a aquestes preguntes

```{r}
# Dur les respostes a l'inici de la segûent sessió
```

1. Com es pot aplicar el format condicional en una columna a partir dels valors en una altra en R? 

2. Quin paquets permeten fer edicions visuals de taules de dades directament des de RStudio? 

3. Quin paquet permet fer joins de taules a partir de cerques per similitud entre cadenes de text definint llindars personalitzats?

4. Quin seria el tipus de resultat esperat d'aquest codi sobre el **dataset b**?:    

```{r eval=FALSE}
dades.w <- dades %>% group_by(MesDerivacioSAIER, SituacioAdministrativa) %>% count() %>%
  spread(SituacioAdministrativa, n)
formattable(dades.w, list(area(col = colnames(dades.w)[-1]) ~ color_tile("transparent", "red")) )
```

