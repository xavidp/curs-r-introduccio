---
title: "Curs R (OMD) - Exemple A: 'La Llista Oblidada'"
subtitle: "Sessió 5"
author: "Xavier de Pedro"
date: "4 d'Abril de 2019"
output: 
  html_document:
    df_print: paged
    toc: yes
    toc_float: yes
    number_sections: yes
    code_folding: show
    keep_md: false
    includes:
      in_header: exercici_ajbcn_capcalera.html
      before_body: exercici_ajbcn_abans.html
      after_body: exercici_ajbcn_despres.html
abstract: |
  Informació del Curs: 
  https://omd-gid.imi.bcn/CursR
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
require(pacman)
p_load(tidyr, dplyr, janitor, stringr)
```
# Objectiu

* analitzar una llista de registres amb informació numèrica, categòrica, textual i amb dates
* posar en pràctica el coneixement après fins a la data en cada moment
* objectiu final: informe en html senzill (sense plantilla de l'Ajuntament de Barcelona) amb resums en taules estàtiques i dinàmiques, gràfics estàtics i dinàmics, i relació de paquets i versions de programari emprats per produir-lo.

Dades:
* Projecte Umbral: https://www.barcelona.cat/umbral/

> Recopilada i actualitzada cada any per UNITED for Intercultural
> Action, una xarxa contra la discriminació formada per 550
> organitzacions a 48 països, The List (La Llista) documenta informació
> relacionada amb les morts de 35.597 persones refugiades i migrants
> (documentació a data 30 de setembre de 2018) que han perdut les seves
> vides dins i fora de les fronteres d'Europa des de 1993 degut a les
> polítiques estatals i els seus defensors. Des de 2007, en
> col·laboració amb artistes i institucions, Banu Cennetoğlu ha
> facilitat versions actualitzades de La Llista utilitzant espais
> públics com panells publicitaris, xarxes de transport i diaris.
> 
> Traduïda per l'Associació La Llista Oblidada i editada per Maike
> Moncayo.
> 
> www.list-e.info
> 
> www.unitedagainstracism.org

Agraïments:

* Pablo Peralta, i Ramon Sanahuja. Direcció d'Atenció i Acollida d'Immigrants. Drets de Ciutadania, Cultura, Participació i Transparència. Ajuntament de Barcelona.
* Banu Cennetoğlu, compilador de la llista.

```{r}
if (!require("pacman")) install.packages("pacman"); require(pacman)
p_load(readxl)
p_load(readr)
# ..\dades\TheList_BCN_nov5_finalCatSpaB_fm2.xlsx = "lallista.xlsx"
getwd()
```

# Carreguem les dades

```{r}
# "md" fa referència a "My Data"
md.orig <- read_xlsx("../dades/a/lallista.xlsx", skip=8)
md <- md.orig
md <- clean_names(md) 
colnames(md.orig)
colnames(md)

# "mde" vol dir "My Data - Edited"
if (exists("mde")) {
  mde <- clean_names(mde) 
  md <- mde 
} else if (file.exists("../dades/a/lallista.editada.csv")) {
  rm(md)
  mde <- read_csv("../dades/a/lallista.editada.csv")
  mde <- clean_names(mde) 
  # md %>% 
  #   mutate(`cause_of_death` = `cause_of_death'`) %>%
  #   select(-causa2, -"x7", -`cause_of_death'`)
  # write_csv(md, "../dades/a/lallista.editada.csv")
  md <- mde
}
```

# Fem una ullada

## Mostrar dataframe

```{r}
md
```


## Columnes actuals

```{r}
colnames(md)
```

## Columna "x7"?

```{r}
head(md$x7)
# Same as:
head(md$"x7")
```

```{r}
is.na(md$"x7")[1:100]
```

```{r}
head(is.na(md$"x7"))
```

```{r}
head(!is.na(md$"x7"))
```

```{r}
which(!is.na(md$"x7"))
```

```{r}
no.na.indexes <- which(!is.na(md$"x7"))
md$"x7"[no.na.indexes]
```

```{r}
table(md$"x7")
```

## Què passa amb aquests registres?

Cal re-ajuntar la informació d'aquesta columna extra "x7" amb alguna columna prèvia?

```{r}
md[no.na.indexes,]
```

Potser no en aquest cas, però veiem com es faria, per a quan sigui necessari fer quelcom similar:

### Fusionant-les (eliminant les columnes antigues)

```{r}
md %>% unite("cause_of_death'", "cause_of_death", "x7", sep=" | ") 
```

### Afegint columna nova fusionada (sense eliminar les columnes antigues)

```{r}
md$"cause_of_death'" <- paste("cause_of_death", "x7") 
head(md)
md$"cause_of_death'" <- paste(md$"cause_of_death", md$"x7", sep=" | ") 
head(md)
```

Tambe podem cridar directament la columna que ens interessa, pel nom:

```{r}
head(md)[ ,"cause_of_death'"]
```

O per la posició numèrica concreta, o relativa, com en aquest cas, que sabem que és la darrera columna:

```{r}
head(md)[,length(colnames(md))]
```


### Fusionat condicional (via BUCLE amb Rcore)

Fusiona les columnes només en els casos en que hi hagi valor. Si no, no hi afegeixis un " | NA" que no aporta res. Amb bucle de les funcions bàsiques de R. 

```{r}
#carreguem paquet petit per mirar temps d'execució
p_load(tictoc)

# Mirem quantes files tenim
nrow(md)
# 4085

# Afegim columna nova buida "causa2"
md$causa2 <- NA

# Fem un bucle sencer amb tot el dataset.
tic("Fusionat AMB Bucle")
for (n in 1:nrow(md)) {
  # Només si hi ha dades a "x7"...., 
  if (!is.na(md$x7[n])) { 
    # afegim valors d'informació fusionada de les dues columnes en columna nova 
    md$causa2[n] <- paste(md$"cause_of_death"[n], md$"x7"[n], sep=" | ") 
  } else {
    # no fem res aquí
  }
}
toc()
#Fusionat AMB Bucle: 0.028 sec elapsed

which(!is.na(md$causa2))

md$causa2[no.na.indexes]

```

### Mateix resultat sense Bucle (via TIDYVERSE)

Fusiona les columnes, i elimina els caracters afegits innecessaiament per als casos en que no hi havia valor. Amb funcions vectoritzades dels paquets del TidyVerse ( https://www.tidyverse.org/ ) 

```{r}
#carreguem paquet petit per mirar temps d'execució
p_load(tictoc, stringr)

tic("Fusionat sense Bucle")
# afegim valors d'informació fusionada de les dues columnes en columna nova 
md2 <- md %>% 
  unite("cause_of_death'", "cause_of_death", "x7", sep=" | ")
toc()
# Fusionat sense Bucle: 0.009 sec elapsed

tic("Espurgant text no desitjat amb str_replace")
md2$`cause_of_death'`  <- str_replace(md2$`cause_of_death'` , pattern = " | NA", replacement ="")
toc()
#Espurgant text no desitjat amb str_replace: 0.014 sec elapsed

md2$`cause_of_death'`[no.na.indexes]

```

# Comentaris a la capçalera de l'arxiu Excel de la LLista Oblidada

```{r}
my.comments <-read_xlsx("../dades/a/lallista.xlsx", n_max=7)
my.comments
```

# Netegem informació de dates

* Quins tenen data en format dd/mm/yy ("31/12/99", "30/12/04", ...)
* Quins tenen data en format XXX NN* ("Apr 00", "Feb 95", "Feb 1994", ...)
* Quins tenen altres formats ("36861", "36455", ...)

## Consultem expressions regulars a stringr

https://stringr.tidyverse.org/articles/regular-expressions.html

## Afegim Rstudio addin per a ajudar-nos amb les Expressions Regulars

regexplain: https://github.com/gadenbuie/regexplain

![regexplain](https://raw.githubusercontent.com/gadenbuie/regexplain/af4fe0988a10f34dc528b4d359b80bb06af7809a/docs/regexplain-selection.gif){width="80%"}


## Selecció amb expressions regulars
```{r}
# Quins tenen data en format yy/mm/dd o similar
# Agafem codi a través del RStudio addin
my.pattern.ddmmyy <- "\\d{0,2}/\\d{2}/(?:\\d{4}|\\d{2})?|\\d{0,2}-\\d{2}-(?:\\d{4}|\\d{2})?|\\d{0,2}\\.\\d{2}\\.(?:\\d{4}|\\d{2})?|(\\b)([A-Za-z]{3,9})(\\s+)([0-9][0-9]*)(,)(\\s+)([0-9]{4})|[0-9]{4}-[0-9]{2}-[0-9]{2}" # perl=TRUE

head(grep(my.pattern.ddmmyy, md$`found_dead`, value=T))
```

```{r}
head(grep(my.pattern.ddmmyy, md$`found_dead`, value=F))
```

```{r}
head(grep(my.pattern.ddmmyy, md$`found_dead`, value=T, invert=T))
```

```{r}
head(grep(my.pattern.ddmmyy, md$`found_dead`, value=F, invert=T))
```

```{r}
# Indexos dels registres que tenen format dd/mm/yy o similar
dead.subset1.ddmmyy.idx <- grep(my.pattern.ddmmyy, md$`found_dead`, value=F, invert=F)

#Els altres
#grep(my.pattern.ddmmyy, md$`found_dead`, value=T, invert=T)

# Indexos dels que tenen altres formats
dead.subset1.anti.idx <- grep(my.pattern.ddmmyy, md$`found_dead`, value=F, invert=T)
# Fem una ullada
rbind(head(md$`found_dead`[dead.subset1.anti.idx]),
      tail(md$`found_dead`[dead.subset1.anti.idx])
      )
```

```{r}
# Comprovem que no ens deixem cap en la partició de casos inicial
nrow(md) - length(dead.subset1.ddmmyy.idx) - length(dead.subset1.anti.idx)
```


```{r}
# Quins tenen data en format YYYY
my.pattern.yyyy <- "\\d{4}" # perl=TRUE
grep(my.pattern.yyyy, md$`found_dead`[dead.subset1.anti.idx], value=T)
```

Tenim alguns que no són estrictament YYYY: Jun 1994, 3010/16, 36455, ...

```{r}
# Que comencin per un número exactament de 4 digits
my.pattern.yyyy <- "^\\d{4}" 
grep(my.pattern.yyyy, md$`found_dead`[dead.subset1.anti.idx], value=T)
```

Tenim alguns que no són estrictament YYYY: 3010/16, 36455, ...

```{r}
# Que comencin i acabin per un número exactament de 4 digits
my.pattern.yyyy <- "^\\d{4}$"
grep(my.pattern.yyyy, md$`found_dead`[dead.subset1.anti.idx], value=T)
```

```{r}
# Els guardem els indexos, que hem de cercar de nou a tot md
dead.subset2.yyyy.idx <- grep(my.pattern.yyyy, md$`found_dead`, value=F)
  
# Els altres
my.pattern.anti.yyyy.ddmmyy <- paste(my.pattern.ddmmyy, my.pattern.yyyy, sep="|")

head(grep(my.pattern.anti.yyyy.ddmmyy, md$`found_dead`, value=T, invert=T))

# Guardem els indexos
dead.subset2.anti.idx <- grep(my.pattern.anti.yyyy.ddmmyy, md$`found_dead`, value=F, invert=T)
```


```{r}
# Els "Mes YY"
my.pattern.mesyy <- "^\\w+ \\d{2}$"
head(grep(my.pattern.mesyy, md$`found_dead`, value=T))
```

```{r}
# Els guardem els indexos, que hem de cercar de nou a tot md
dead.subset3.mesyy.idx <- grep(my.pattern.mesyy, md$`found_dead`, value=F)
  
# Els altres
my.pattern.anti.mesyy.yyyy.ddmmyy <- paste(my.pattern.ddmmyy, my.pattern.yyyy, my.pattern.mesyy, sep="|")

# Quins tenen altres formats
# Exemple: 36861, 36455
grep(my.pattern.anti.mesyy.yyyy.ddmmyy, md$`found_dead`, value=T, invert=T)

# Guardem els indexos dels casos mes atípics
dead.subset3.anti.idx <- grep(my.pattern.anti.mesyy.yyyy.ddmmyy, md$`found_dead`, value=F, invert=T)
```

```{r}
# Edició a manopla de valors a RStudio
# No ho exeecutis sempre que s'executa tot el codi, només quan es cridi a ma
if (F) {
  p_load(editData)
  mde <- editData(md)
}
```

## Creem noves columnes de Mes i Any de les morts

```{r}
p_load(lubridate)
mde <- mde %>%
  # subset 1: dd/mm/yy
  mutate(d.1.yy = year(as.POSIXlt(`found_dead`, format="%d/%m/%Y"))) %>%
  mutate(d.1.mm = sprintf("%02d", month(as.POSIXlt(`found_dead`, format="%d/%m/%Y"))))

mde <- mde %>%
  # subset 2: yyyy
  mutate(d.2.yy = year(as.POSIXlt(`found_dead`, format="%Y"))) %>%
  mutate(d.2.mm = "00")

mde <- mde %>%
  # subset 3: mesyy
  mutate(d.3.yy = str_extract(`found_dead`, "\\d{2}$")) %>%
  mutate(d.3.mm = sprintf("%02d", match(str_extract(`found_dead`, "^\\w+"), month.abb)))

if (F) {
# Test merging years
  mde %>%
    mutate(d.yy = sprintf("%02d", coalesce(as.double(d.3.yy), d.2.yy, d.1.yy))) %>%
    select("found_dead", d.yy, d.3.yy, d.2.yy, d.1.yy) %>%
    filter(d.yy < 04)
}

# Merge years
mde <- mde %>%
  mutate(d.yy = sprintf("%02d", coalesce(as.double(d.3.yy), d.2.yy, d.1.yy))) 

if (F) {
  # Test merging months
  mde %>%
    mutate(d.mm = sprintf("%02d", coalesce(as.double(d.1.mm), as.double(d.3.mm), as.double(d.2.mm)))) %>%
    select("found_dead", d.mm, d.3.mm, d.2.mm, d.1.mm)
}

# Merge months
mde <- mde %>%
  mutate(d.mm = sprintf("%02d", coalesce(as.double(d.1.mm), as.double(d.3.mm), as.double(d.2.mm))))

# Convert years into 4 digits
if (F) {
  # test the conversion
  mde %>%
    mutate(d.yyyy = case_when(
      d.yy < 20 ~ paste0("20", d.yy),
      d.yy > 20 ~ paste0("19", d.yy)
    ))  %>%
    select("found_dead", d.yy, d.yyyy)
}

# do the conversion
mde <- mde %>%
  mutate(d.yyyy = case_when(
    d.yy < 20 ~ paste0("20", d.yy),
    d.yy > 20 ~ paste0("19", d.yy)
  ))
  
# Abbreviated names of months are stored in R in month.abb object

# Afegim columna yyyy-mm
mde <- mde %>%
  mutate(d.yyyymm = str_c(d.yyyy, d.mm, sep="-"))

table(mde$d.yyyymm)

# Eliminem variables temporals per a l'obtenció de yyyy-mm
mde <- mde %>%
  select("d.yyyymm", c(colnames(md)[1:6], "d.yyyy", "d.mm"))
```

## Agregació per mesos

Recompte de registres per mes

```{r}
# Compta registres de cada mes
mde %>%
  group_by(d.yyyymm, d.yyyy, d.mm) %>%
  count

```

Suma de morts reportades per mes

```{r}
# Suma tots els morts ("number") per a cada mes enlloc de contar només en quantes registres s'han reportat
mde.morts.x.mes <- mde %>%
  group_by(d.yyyymm, d.yyyy, d.mm) %>%
  summarize(suma = sum(number, na.rm = TRUE))
mde.morts.x.mes
```
