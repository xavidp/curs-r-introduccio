---
title: "Exemple A: 'La Llista Oblidada'"
subtitle: "Sessió 01 - Curs R (OMD)"
author: "Xavier de Pedro"
date: "5 de Març de 2019"
output: 
  html_document:
    df_print: paged
    toc: yes
    toc_float: yes
    number_sections: yes
    code_folding: show
    keep_md: false
    includes:
      in_header: exercici_ajbcn_capcalera.html
      before_body: exercici_ajbcn_abans.html
      after_body: exercici_ajbcn_despres.html
abstract: |
  Informació del Curs: 
  https://omd-gid.imi.bcn/CursR
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
# Objectiu

* analitzar una llista de registres amb informació numèrica, categòrica, textual i amb dates
* posar en pràctica el coneixement après fins a la data en cada moment
* objectiu final: informe en html senzill (sense plantilla de l'Ajuntament de Barcelona) amb resums en taules estàtiques i dinàmiques, gràfics estàtics i dinàmics, i relació de paquets i versions de programari emprats per produir-lo.

Dades:
* Projecte Umbral: https://www.barcelona.cat/umbral/

> Recopilada i actualitzada cada any per UNITED for Intercultural
> Action, una xarxa contra la discriminació formada per 550
> organitzacions a 48 països, The List (La Llista) documenta informació
> relacionada amb les morts de 35.597 persones refugiades i migrants
> (documentació a data 30 de setembre de 2018) que han perdut les seves
> vides dins i fora de les fronteres d'Europa des de 1993 degut a les
> polítiques estatals i els seus defensors. Des de 2007, en
> col·laboració amb artistes i institucions, Banu Cennetoğlu ha
> facilitat versions actualitzades de La Llista utilitzant espais
> públics com panells publicitaris, xarxes de transport i diaris.
> 
> Traduïda per l'Associació La Llista Oblidada i editada per Maike
> Moncayo.
> 
> www.list-e.info
> 
> www.unitedagainstracism.org

Agraïments:

* Pablo Peralta, i Ramon Sanahuja. Direcció d'Atenció i Acollida d'Immigrants. Drets de Ciutadania, Cultura, Participació i Transparència. Ajuntament de Barcelona.
* Banu Cennetoğlu, compilador de la llista.

```{r}
if (!require("devtools")) install.packages("devtools"); require(devtools)
if (!require("pacman")) install_version("pacman", version = "0.4.1"); require(pacman)
p_load(readxl)
# ..\dades\TheList_BCN_nov5_finalCatSpaB_fm2.xlsx = "lallista.xlsx"
getwd()
```
# Carreguem les dades

```{r}
# "md" fa referència a "My Data"
p_load(readxl)
md <- read_xlsx("../dades/lallista.xlsx", skip=8)
```

# Comentaris a la capçalera de l'arxiu Excel de la LLista Oblidada

```{r}
# "mc" fa referència a "My Comments"
mc <-read_xlsx("../dades/lallista.xlsx", n_max=7)
mc
```

# Fem una ullada a "md" (My Data)

# Mostrar dataframe

```{r}
md
```


# Columnes actuals

```{r}
colnames(md)
```

# Fem una taula

Una taula estàtica però cercable

## Taula cercable - amb DT

```{r}
p_load(DT)
datatable(md)
```

# Fem un gràfic

# Wordcloud (png)

```{r}
p_load(tm, wordcloud, RCurl, SnowballC, XML, RColorBrewer)

source('http://www.sthda.com/upload/rquery_wordcloud.r')

res <- rquery.wordcloud(x=md$"cause of death", 
                        type="text", 
                        min.freq = 5, 
                        max.words = 200, 
                        excludeWords=c("found","centre","way"), 
                        colorPalette="Dark2"
                        ) 
```

# Paquets i versions
Relació de paquets i versions de programari R emprats per produir-lo.

```{r}
cat("Sys.info() : \n")
cat("--------------------\n")
data.frame(Sys.info())
cat("\n\nsessionInfo() : \n")
cat("--------------------\n")
sessionInfo()
cat("--------------------\n")

```

# Opcional: extraiem codi de l'arxiu .Rmd en arxiu .R

```{r, eval=FALSE}
p_load(knitr)
#Extract R code only
purl("exemple.a.01.Rmd")
# Extract R code and also include documentation
purl("exemple.a.01.Rmd", output = "exemple.a.01.v2.R", documentation = 2)
```


