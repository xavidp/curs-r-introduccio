# Derived from https://stackoverflow.com/a/11561793/4112288
# Thanks to Dirk Eddelbuettel
#
# Sembla que hi ha dues formes de fer "query" a CRAN per consola. I no totes dues retornen el mateix conjunt de resultats (no semblen estar igual d'actualitzades?)
# (1) emprant CRANsearcher::cran_inventory()
# (2) emprant getPackagesInfo() carregada des d'arxiu R (derivada de https://stackoverflow.com/a/11561793/4112288 )
# Aquí sota hi ha la funció retocada per a getPackagesInfo()
require("tools")
getPackagesInfo <- function() {
  contrib.url(getOption("repos")["CRAN"], "source") 
  description <- sprintf("%s/web/packages/packages.rds", 
                         getOption("repos")["CRAN"])
  con <- if(substring(description, 1L, 7L) == "file://") {
    file(description, "rb")
  } else {
    url(description, "rb")
  }
  on.exit(close(con))
  db <- readRDS(gzcon(con))
  rownames(db) <- NULL
  
  db[, c("Package", "Title", "Description", "Version")]
}