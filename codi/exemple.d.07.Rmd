---
title: "Exemple D: 'Validar signatures per districte'"
subtitle: "Sessió 07 - Curs R (OMD)"
author: "Xavier de Pedro"
date: "23 d'Abril de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  html_document:
    df_print: paged
    toc: yes
    toc_float: yes
    number_sections: yes
    code_folding: show
    keep_md: false
    includes:
      in_header: exercici_ajbcn_capcalera.html
      before_body: exercici_ajbcn_abans.html
      after_body: exercici_ajbcn_despres.html
abstract: |
  Informació del Curs: 
  https://omd-gid.imi.bcn/CursR
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
if (!require("pacman")) install.packages("pacman"); require(pacman)
```
# Objectiu: **Resoldre el cas plantejat**

Ens donen dins un arxiu d'Excel un joc de dades de signants d'una iniciativa ciutadana per **demanar l'alliberament dels Peixos Pacífics** (campanya del _llaç blau_). 
Heu de validar que aquestes persones són empadronades a la nostra ciutat al districte 1 del qual provenen les dades: **Quantes signatures són valides?** 

* Conjunts de Dades
    * **Dades dels signants (and dni i/o nie)**: 
        * Carpeta `./dades/d/` > `signants.xls` > Full " _Signants_ "
    * **Dades del padró (amb nif_nie i codi de vivenda)**: 
        * Carpeta `./dades/d/` > `signants.xls` > Full " _Padró_ "
    * **Dades de les vivendes (amb codi vivenda i districte)**: 
        * Carpeta `./dades/d/` > `signants.xls` > Full " _Vivendes_ "


# Carreguem paquets

```{r}
if (!require("devtools")) install.packages("devtools"); require(devtools)
if (!require("pacman")) install_version("pacman", version = "0.4.1"); require(pacman)
p_load(tidyverse, readxl, janitor, fuzzyjoin)
```

# Llegim dades

Llegeix l'arxiu de dades `signants.xls` i assigna el seu contingut a tres dataframes `signants`, `padro`, i `vivendes`

## Signants

```{r}
signants <- read_xls("../dades/d/signants.xls", sheet="Signants")
signants <- clean_names(signants)
signants
```

### Correcció de noms de columnes

Correció dels noms de columnes, que havien quedat una línia per sota de l'esperat (per què hi havia una primera línia de comentaris a sobre):

```{r}
# Aquí podríem tornar a llegir les dades originals saltant-nos la primera fila
#signants <- read_xls("../dades/d/signants.xls", sheet="Signants", skip = 1)

# O bé podem emprar la funció `row_to_names()` del paquet `janitor`, que converteix en els noms de columnes el numero de fila que s'indiqui (eliminant les files que estiguin per sobre):
signants <- janitor::row_to_names(signants, 1)
signants
```

Per major consistència amb les persones que hagin llegit l'excel saltant-se la primera línia al primer full per ser de comentaris, assignem el nom de columna a mà que mancava per a la quarta columna de les dades inicials, com a `x4`, que seria el nom que donaria la funció clean_names() aplicada sobre el dataframe equivalent: 

```{r}
colnames(signants)[4] <- "x4"
```

Netegem els noms de columnes de nou ara:

```{r}
signants <- clean_names(signants)
signants
```

### Hi ha duplicats que calgui eliminar?

Hi ha diverses maneres de cercar duplicats.  No totes donen la mateixa informació, ni són igual d'eficients amb conjunts de dades grans.

Amb R core, `duplicated()`:
```{r}
# Podem obetenir un vector de TRUE/FALSE amb les files del dataframe que són duplicats d'alguna altra :
duplicated(signants)

# També podem aprofitar aquest vector per mostrar les files on hi ha les còpies:
signants[duplicated(signants), ]
```

I ens podríem quedar amb els registres que no són duplicats amb `unique()`:

```{r}
unique(signants)
```

Ara bé, sembla que `Tidyverse` té la funció `distinct()` que és més eficient ( _que la funció "unique()" de R base_ ) per eliminar duplicats:

```{r}
signants <- distinct(signants)
signants
```

Cal tenir present que de vegades ens pot interessar una forma fàcil de mostrar les diferents files que tenen columnes. Una forma fàcil és amb la funció `get_dupes()` del paquet `Janitor`, que es pot emprar sense arguments o bé especificant un subconjunt de variables per a les que es vol cercar si hi ha duplicats, i quantes repeticions n'hi ha de cada cas:

```{r}
get_dupes(signants)
```

## Padró

```{r}
padro <- read_xls("../dades/d/signants.xls", sheet="Padró", skip=0)
padro <- clean_names(padro)
padro
```

### Hi ha duplicats que calgui eliminar?

Revisem duplicats (files senceres):

```{r}
get_dupes(padro)
```

Revisem duplicats de nif_nie, malgrat el nom-cognoms pugui ser diferent.

```{r}
get_dupes(padro, nif_nie)
```

```{r}
padro <- distinct(padro, nif_nie, .keep_all = T)
padro
```

## Vivendes

```{r}
vivendes <- read_xls("../dades/d/signants.xls", sheet="Vivendes")
vivendes <- clean_names(vivendes)
vivendes
```

# Mirem quins dels signataris són presents al padró

Per esbrinar-ho, farem un `join` dels dataframes de signants i padró per la clau (variable) comú entre ambdós. Primer hem de revisar quines columnes hi ha a cada dataframe:

```{r}
list(colnames(signants),
      colnames(padro) )
```

Ara ja podem fer el join. Veiem que hi ha diversos tipus de joins, i a nosaltres ara ens interessa el `inner_join()`, atès que ens retorna totes les files del primer dataframe que tenen troballa al segon (per a la clau comú) i amb les columnes afegides presents al segon dataframe:

Fem un **inner_join()**:

```{r}
signants.empadronats <- inner_join(signants, padro, by = c("dni_i_o_nie" = "nif_nie"))
signants.empadronats
```

Se'ns han afegit totes les columnes extra del segon dataframe.

Ens quedarem només amb les columnes mínimes que ens interessen dels dos dataframes, per major llegibilitat dels resultats significatius:

```{r}
signants.empadronats <- signants.empadronats %>%
  select(c(1:3,8))
signants.empadronats
```

## I ara cerquem quines d'aquestes vivendes són al districte 01

Per tant, fem el join amb Vivendes per saber els seus districtes.

Fem un altre **inner_join()**:

```{r}
signants.empadronats.districtes <- inner_join(signants.empadronats, vivendes, by = "codi_vivenda") 
signants.empadronats.districtes
```

I ara eliminem la columna adreça ("adreca") i la del codi_vivenda (que només l'hem necessitat per trobar el districte) per que no ens interessen en els resultats que necessitem, i finalment ens quedem només amb els registres que persones signants empadronades a la ciutat que viuen només al districte 1 que ens han demanat.

```{r}
resultats <- signants.empadronats.districtes %>%
  select(-adreca,
         nom = nom.x,
         nif_nie = dni_i_o_nie) %>%
  filter(districte == "01")
resultats
```

Fixem-nos que com la variable districte esta desada en format caracter, i amb dos "dígits", no podem filtrar con el número 1 (`districte == 1`), ni el número 01 (`districte == 01`), sinó contra el text 01, indicat envoltant-lo entre cometes (`districte == "01"`).

Per tant, **en teoria** en tenim **`r nrow(resultats)` signant/s vàlid/s** per al districte demanat:
`r knitr::kable(resultats)`

**Ho donem per valid?** Continua llegint, que se'ns han escapat algunes coses rellevants...

# Continuació (i): Cerca per Nom i Cognoms donaria resultats similars?

Per esbrinar-ho, farem un `join` dels dataframes de signants i padró per una nova clau comú que anomenarem ncc ("Nom Cognom1 Cognom2"), i que serà la nova clau comú entre ambdós dataframes 

Tornem a revisar quines columnes hi ha a cada dataframe:

```{r}
list(colnames(signants),
      colnames(padro) )
```

Afegim `ncc` per al primer dataframe:
```{r}
signants <- signants %>%
  unite(ncc, nom, cognoms, sep=" ", remove = FALSE)
signants
```

Afegim `ncc` per al segon dataframe:
```{r}
padro <- padro %>%
  unite(ncc, nom, cognom1, cognom2, sep=" ", remove = FALSE)
padro
```

Ara ja podem fer el join. Veiem que hi ha diversos tipus de joins, i a nosaltres ara ens interessa el `inner_join()`, atès que ens retorna totes les files del primer dataframe que tenen troballa al segon (per a la clau comú) i amb les columnes afegides presents al segon dataframe:

Fem un **inner_join()**:

```{r}
inner_join(signants, padro, by = c("ncc" = "ncc")) %>% 
  select(-x4, -nom.x, -cognoms, -nom.y, -cognom1, -cognom2)
```

Aquí veiem que hi havia altres casos de falsos negatius probablement: 

* nif_nie considerats diferents quan haurien d'haver estat considerats idèntics (diferències només de minúscules i majúscules en la lletra del nif)
    * **Cal convertir a majúscules les lletres dels nif_nie dels signants**
* mantenir `NA` de **nif_nie** als signants ha produït una coincidència artificial amb el registre del padró que no té nif_nie tampoc.
    * Cal eliminar els NA dels registres per evitar coincidències aritificials amb els registres dels signants

# Continuació (ii): Cerca per similitud
Eliminem els NA de nif dels registres
```{r}
padro.nona <- padro %>% filter(!is.na(nif_nie))
signants.nona <- signants %>% filter(!is.na(dni_i_o_nie))
```

Farem una cerca per similitud fixant llindar a les distàncies entre les cadenes de text a comparar al join, amb funcions del paquet `fuzzyjoin`.

## Cerca per similitud de nif_nie

```{r}
stringdist_inner_join(signants.nona, padro.nona, by = c("dni_i_o_nie" = "nif_nie"), method = "osa", max_dist=1, ignore_case=F) %>% 
  filter(codi_vivenda == "E") %>% 
  select(ncc.x, dni_i_o_nie, nif_nie, ncc.y, codi_vivenda, -x4, -nom.x, -cognoms, -nom.y, -cognom1, -cognom2)
```

## Cerca per similitud de ncc

```{r}
stringdist_inner_join(signants.nona %>% distinct(ncc, .keep_all = T),
                      padro.nona, 
                      by = "ncc" , method = "osa", max_dist=1, ignore_case=F) %>% 
  filter(codi_vivenda == "E") %>% 
  select(nom.x, cognoms, dni_i_o_nie, nif_nie, nom.y, cognom1, cognom2, codi_vivenda, -x4)
```
Veiem que entre els dos conjunts de resultats de cerca per similitud per `nif_nie` o `ncc` acceptant una màxima distància d'1 unitat (aprox. un caràcter de diferència), hi ha el registre de l' _"Aytor Tilladepa Tatas"_, amb el dni_nie diferents per dos caracters. Podem comprovar la lletra del NIF

```{r}
source("funcio.fix.nif.letter.R")
fix.nif.letter("07654321I")
```

I comprovem que coincideix amb la del registre equivalent del padró. Per tant, podríem donar aquests 4 resultats com a vàlids.


# Fi

## Paquets i versions
Relació de paquets i versions de programari R emprats per produir-lo.

```{r}
cat("Sys.info() : \n")
cat("--------------------\n")
data.frame(Sys.info())
cat("\n\nsessionInfo() : \n")
cat("--------------------\n")
sessionInfo()
cat("--------------------\n")

```

