---
title: "Exercici de la Sessió 10"
subtitle: "Curs R (OMD)"
author: "Xavier de Pedro"
date: "14 de Maig de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css 
tags:
- Exercici
- Curs
- R
- Formació
always_allow_html: yes
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Tasques

1. Fer la feina col·laborativa indicada a `10.guia.vis.gencat.Rmd`
1. Omplir l'enquesta http://omd-gid.imi.bcn/CursR#enquesta
    * _si no hi podeu accedir per ser fora de la xarxa de l'ajuntament, envieu-nos un correu-e a omd-gid@bcn.cat i us ho passem en document adjunt editable_
1. Veure què tenim previst fer a la sessió 11 (extra, el dijous 23 de maig de 2019): 
    * `codi/11.extra.pdf`
1. Confirmar (si no ho heu fet encara) si teniu intenció d'assistir a la sessió 11 (extra).

