---
title: "Sessió 9: Gràfics i Mapes (2a part) - SHP"
subtitle: "Sessió 8: Gràfics i Mapes (1a part)"
institute: 'Oficina Municipal de Dades (OMD). Ajuntament de Barcelona'
author: Xavier de Pedro Puente  (OMD-AjBCN)
date: "7 de Maig de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css 
always_allow_html: yes
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introducció i exercicis
Veure: https://cengel.github.io/rspatial/2_spDataTypes.nb.html 

I també: https://enrdados.netlify.com/post/manual-de-sf-para-sig/

# Revisió en una CRAN Task View: Analysis of Spatial Data

https://cran.r-project.org/web/views/Spatial.html

# Exemples de l'Ajuntament de Barcelona

```{r}
# Informació derivada a partir d'script base de Marc Adzerias Forn. OMD-GID. AjBCN

# Es poden agafar els SHP de:
#  K:\QUOTA\OMD\INTEGRACIO\dades\BasesGrafiques\Shp Divisions Administratives a l'OMD 
# o de
#  CartoBCN: https://w20.bcn.cat/CartoBCN/

#K:\QUOTA\OMD\INTEGRACIO\dades\BasesGrafiques\Shp Divisions Administratives

shp.path <- file.path("K:","QUOTA","OMD","INTEGRACIO","dades","BasesGrafiques","Shp Divisions Administratives","BCN_Districte_ETRS89_SHP.shp")

# --- llibreries -----------------------------------------------#
if(!require(rgdal))    install.packages("rgdal")    ; library(rgdal)
if(!require(maptools)) install.packages("maptools") ; library(maptools)
if (!require(gpclib))  install.packages("gpclib", type="source")
if(!gpclibPermit()) print("cal instal·lar la versió correcta de Rtools: https://cran.r-project.org/bin/windows/Rtools/")

# --- lectura de shp -------------------------------------------#
mapa.DTE <- readOGR(dsn = shp.path, encoding = "UTF-8")  # llegim fitxer .shp

# nota: mapa_DTE -> objecte classe SpatialPolygonsDataFrame
# 2 parts -> vectors amb dades referides als poligons i poligons
mapa.DTE$nou.atribut <- c(1001,1002,1003,1004,1005,1006,1007,1008,1009,1010)

plot(mapa.DTE)
text(x=mapa.DTE$Coord_X, y=mapa.DTE$Coord_Y, labels = mapa.DTE$nou.atribut)
```

# Forma via sf de passar de utm a latlong (INI) ---------------------

```{r}
MOV.geo <- MOV.utm


# Imputem el valor 0 a tots els NA de coordenades UTM (ED50) per a que la conversió a coord geogràfiques no s'aturi
MOV.geo <- impute_zero(MOV.geo)
# que en coordenades geogràfiques seràn  long=-1.489264, lat=0.00000 (quan UTM venien en datum potsdam/ED50)

MOV.geo.crs <- st_as_sf(MOV.geo, coords = c("X_UTM","Y_UTM"), crs = 25831) # Convert data frame into sf object
# 258xx is for UTM coordinates, where “xx” is the "huso" (28,29,30 or 31 in our usual cases).
st_crs(MOV.geo.crs) # Set or replace/retrieve coordinate reference system from object
# Coordinate Reference System:
#   EPSG: 25831 
#   proj4string: "+proj=utm +zone=31 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
#(ETRS89 expressat en coordenades projectades UTM)

MOV.geo.wgs84 <- MOV.geo.crs
MOV.geo.wgs84 <- st_transform(MOV.geo.wgs84, crs = 4326) # Transform or convert coordinates of simple feature
st_crs(MOV.geo.wgs84)
# Coordinate Reference System:
#   EPSG: 4326 
#   proj4string: "+proj=longlat +datum=WGS84 +no_defs"
#(Area Mundial expressat en coordenades geografiques (latlong))

plot(st_geometry(MOV.geo.wgs84))

# Explorar 
# st_write (XXX)
```
