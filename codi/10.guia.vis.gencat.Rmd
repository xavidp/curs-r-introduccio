---
title: "Curs d`R"
subtitle: "Tasca suggerida: Codi R per a la `Guia de Visualització de Dades` (Gencat 2018)"
institute: 'Oficina Municipal de Dades (OMD). Ajuntament de Barcelona'
author: Xavier de Pedro Puente  (OMD-AjBCN)
date: "14 de Maig de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css 
always_allow_html: yes
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

---

# Guia de Visualització de Dades (CC-by 2018 GenCat)

<div class="col2-left">
Més enllà de les eines concretes a emprar, cal saber quin tipus de visualització és més adequada per a cada necessitat. 

La **Guia de Visualització de Dades** (compartida amb llicència oberta _Creative Commons-Atribució_/CC-by 2018, Generalitat de Catalunya, 101pp.) és un gran exemple de material d'ajuda de referència: http://gen.cat/guiavisdades . [Més informació](https://web.gencat.cat/ca/actualitat/detall/20181121_Nova-Guia-de-visualitzacio-de-dades)

I amb **R**? Posar en aquest document (sota) **enllaços a exemples de codi per cada tipus de visualització**, via https://www.data-to-viz.com
</div><div class="col2-center"> </div><div class="col2-right">
![](../grafics/Guia_visu_dades_00_portada.png){width=50%} 

![](../grafics/data-to-viz_homepage_01_violin.png){width=50%}

</div>

---

# Tipus de Visualitzacions de Dades

![](../grafics/Guia_visu_dades_07_grafics_1-4_tipus.png){width=50%}![](../grafics/Guia_visu_dades_08_grafics_5-7_tipus.png){width=50%}

* Tasques:
    1. **Poseu el vostre nom** en alguna de les següents diapositives, **al costat del tipus de gràfic que no tingui persona encarregada**
        * per indicar que us voleu encarregar vosaltres de cercar un enllaç d'on trobar un exemple de codi.
    1. I **feu commit** al repositori de gitlab (resolent els problemes que us trobeu en cada moment! ;-)
        * https://gitlab.com/radup/curs-r-introduccio-demo-git

---

# Tipus: 4.1. _Comparacions_

![](../grafics/Guia_visu_dades_14_s4_1_comparacions_1-4_tipus.png){width=45%} ![](../grafics/Guia_visu_dades_15_s4_1_comparacions_5-7_tipus.png){width=45%} 

* Gràfic de barres:
* Gràfic de barres agrupdades:
* Gràfic d'intensitat de colors:
* Gràfic de marques:
* Gràfic de barres apilades:
* Gràfic de radar
* Gràfic múltiple:
* Altres:

---

# Tipus: 4.2. _Tendències_

![](../grafics/Guia_visu_dades_16_s4_2_tendencies_1-3_tipus.png){width=45%} 

* Gràfic de línies:
* Gràfic de pendents:
* Mini-gràfic de línia:
* Altres:

---

# Tipus: 4.3. _Mapes_

![](../grafics/Guia_visu_dades_17_s4_3_mapes_1-2_tipus.png){width=45%} 

* Mapa de coropletes:
* Mapa de simbols proporcionals:
* I altres gràfics geoposicionats sobre mapa.
    * `leaflet.minicharts`: 
* Altres:

---

# Tipus: 4.4. _Parts d'un total_

![](../grafics/Guia_visu_dades_18_s4_4_parts_d_un_total_1-3_tipus.png){width=45%} 

* Gràfic de sectors:
* Pictograma:
* Mapa d'arbre:
* Altres:

---

# Tipus: 4.5. _Distribucions_

![](../grafics/Guia_visu_dades_19_s4_5_distribucions_1-2_tipus.png){width=45%} 

* Histograma:
* Diagrama de caixa:
* Altres:

---

# Tipus: 4.6. _Correlacions_

![](../grafics/Guia_visu_dades_20_s4_6_correlacions_1-3_tipus.png){width=45%} 

* Gràfic de dispersió:
* Gràfic de bombolles:
* Coordenades paral·leles:
* Altres:

---

# Tipus: 4.7. _Connexions, relacions i xarxes_

![](../grafics/Guia_visu_dades_21_s4_7_conexions_relacions_xarxes_1-2_tipus.png){width=45%} 

* Diagrama de node-aresta:
* Diagrama de Sankey:
* Altres:

---

# Algun altre suggeriment per als exemples de la Guia amb R?

1. Com afegir sobre gràfics els casos en que hi ha significació estadística?
    * Paquet [`ggsignif`](https://cran.r-project.org/package=ggsignif): [Vinyeta](https://cran.r-project.org/web/packages/ggsignif/vignettes/intro.html)
1. Com fer versions adaptades a publicacions:
    * Paquet [`ggpubr`](https://cloud.r-project.org/package=ggpubr): [Vinyeta](https://rpkgs.datanovia.com/ggpubr/index.html)
1. ...
1. ...
1. ...


---

# Agraïments

* Generalitat de Catalunya, per la **Guia de Visualització de Dades** (CC-by 2018). Veure: http://gen.cat/guiavisdades . [Més informació](https://web.gencat.cat/ca/actualitat/detall/20181121_Nova-Guia-de-visualitzacio-de-dades)
* Companys del Departament de Gestió i Integració de dades de la OMD, AjBCN
* Persones assistents al [Curs d'Introducció a **R Modern**](https://gitlab.com/radup/curs-r-introduccio) de la OMD . Ajuntament de Barcelona. ;-)

---

# ANNEXOS

---

# Principis bàsics sobre visualitzacions de dades

![](../grafics/Guia_visu_dades_01_que_1_es_explorar.png){width=45%} ![](../grafics/Guia_visu_dades_02_que_2_es_analitzar.png){width=45%} 
 
![](../grafics/Guia_visu_dades_03_que_3_es_explicar.png){width=45%} 



---

# Passos previs a la visualització de dades

![](../grafics/Guia_visu_dades_04_com_1_definint.png){width=45%} ![](../grafics/Guia_visu_dades_05_com_2_preparant.png){width=45%} 
 

![](../grafics/Guia_visu_dades_06_com_3_dissenyant.png){width=45%} 



---

# Tipus: 5.2. _Principis de Disseny: color_

![](../grafics/Guia_visu_dades_22_s5_2_colors_exemple_heatmap_table_monocromatica.png){width=45%} 

![](../grafics/Guia_visu_dades_23_s5_2_colors_escala_dicotomica_gradient_3_colors.png){width=45%} 

![](../grafics/Guia_visu_dades_24_s5_2_colors_escala_categorica_d3js.png){width=45%} 

---

# Tipus: 5.3. _Principis de Disseny: forma_

![](../grafics/Guia_visu_dades_25_s5_3_forma__1-9_tipus.png){width=45%} 


---

# Tipus: 5.4. _Principis de Disseny: interacció_

![](../grafics/Guia_visu_dades_26_s5_4_interaccio.png){width=45%} 


---

# Tipus: 5.5. _Principis de Disseny: "Menys és més"_

![](../grafics/Guia_visu_dades_27_s5_5_menys_eS_mes__cita_edward_tufte.png){width=100%} 


---

# Tipus: 5.2. _Principis de Disseny: Equilibri entre funcionalitat i estètica_

![](../grafics/Guia_visu_dades_28_s5_5_equilibri_funcionalitat_i_estetica.png){width=80%} 

---

## Exemples avançats (I)

![](../grafics/Guia_visu_dades_11_exemple_1_complex.png){width=60%} 

---

## Exemples avançats (II)

![](../grafics/Guia_visu_dades_12_exemple_2_heatmap_table.png){width=60%} 


---

## Exemples avançats (III)

![](../grafics/Guia_visu_dades_13_exemple_3_infografia.png){width=80%} 

