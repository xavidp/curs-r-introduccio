---
title: "Curs d`R"
subtitle: "Sessió 6: Analitzar dades (3a part)"
institute: 'Oficina Municipal de Dades (OMD). Ajuntament de Barcelona'
author: Xavier de Pedro Puente  (OMD-AjBCN)
date: "11 d'Abril de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css 
always_allow_html: yes
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
require(pacman)
p_load(tidyverse, readxl, janitor, purrr, purrrlyr, future.apply, tictoc)
```


# Objectius per avui

1. Comentaris inicials
    1. Recordatori: Inici i muntatge USB
    1. Actualitzar repositori del curs via **git**
    1. Resoldre **dubtes** sorgits entre sessions presencials
1. Continuar amb **Anàlisi de dades** (3a part)
    1. Enllestir resolució **exercicis (i exemples) anteriors**
        * Exemple resolt sobre dataset **b** - **SAIER**: `exemple.b.06.Rmd`
        * Exemple resolt sobre **Cas** amb dataset **d** - **Validació DNIs**: `exemple.d.06.Rmd`
1. Aprendre nous conceptes i competències:
    1. Aplicar funcions sobre objectes d'R (amb **`apply()`** i relacionats)
1. Fer **exercici** sessió 6

---

# Comentaris inicials: Inici i muntatge USB
<center>
![](../grafics/slax_ajbcn_cursR_00_menu_progs.png){ width=30% } ![](../grafics/slaxusb_munta_usb_01_execute_sh.png){ width=60% } 
</center>

---

# Comentaris inicials: Actualitzar repositori del curs via git
* Muntar disc usb (executar `munta_usb.sh`)
* Obrir RStudio
* Obrir Projecte RStudio `curs-r-introduccio-omd` fet el dia anterior
* Clicar sobre pestanya **Git** > **Pull** (fletxa cap avall) 

<center>![](../grafics/rstudio_git_pull_canvis_curs_04_ok_pull_al_dia.png){ width=70% }</center>

---

# Algun conflicte en baixar canvis?{.smaller}
* Si falla el procés de descarregar els canvis dels arxius canviats (`git pull`):
    * Desa els arxius modificats amb un altre nom d'arxiu personal
    * Reverteix els canvis dels arxius locals dels apunts
    * Tornar a clicar a la fletxa fer descarregar canvis (`git pull`)

<center>![](../grafics/rstudio_git_pull_canvis_curs_00.png){ width=25% } ![](../grafics/rstudio_git_pull_canvis_curs_02_local_changes_merge_failed.png){ width=60% }</center>
<center>![](../grafics/rstudio_git_pull_canvis_curs_03_revert_local_changes_before_pull.png){ width=30% }</center>

---

## Enllestir resolució exercicis anteriors

Veure: `exercici.05.Rmd` (i/o **exercici.05.pdf**)


.

## Continuar amb Anàlisi de dades (3a part)

* Exemple resolt sobre dataset **b**: **SAIER**
    * `exemple.b.06.Rmd`
* Exemple resolt sobre dataset **d**: **Validació DNIs**
    * `exemple.d.06.Rmd`

---

# Aprendre nous conceptes i competències
* **apply()** (de l'`R base`)
* purrr::`map()` (del `Tidyverse`)
* versions de treball **en paral.lel** 
  (ordinadors multicore, clústers de computació, ...)
    * **`future.apply()`** (evolució dels `*apply()` amb paral·lelització)
    * furrr::`map()` (evolució dels `map()` amb paral·lelització)
    * altres solucions (n'hi ha moltes aproximacions!)

---

# Bucles vs apply
Quan es treballa sobre vectors/dataframes grans o llistes llargues es poden fer bucles per fer tota la feina, amb les funcions `for` o `while`. 

Però **el bucle sol ser força millorable en termes d'eficiència i rendiment** si passem a emplar funcions de la família dels `apply` (o altres mètodes més optimitzats, com funcions vectoritzades, o eventualment tasques en paral.lel per a datasets molt grans, ...):

* `apply`: aplica una funció sobre els marges d'una matriu (per files o columnes).
* `lapply`: recorre els elements d'una llista i avaluar una funció en cada un d'ells de forma consecutiva.
* `sapply`: igual que lapply, però intenta simplificar el resultat.
* `tapply`: aplica una funció sobre els subconjunts d'un vector.
* `mapply`: és una versió multivariada de lapply.

---

## apply()

Aplica una funció a una matriu, llista o vector que se le pasi com a paràmetre.

* _Argument 1_: matriu, llista o vector
* _Argument 2_: posa `1` per aplicar la funció sobre les files i `2` per fer-ho sobre les columnes
* _Argument 3_: funció a aplicar.

```{r}
# Escriu el codi necessari en aquest chunk d'R:
matriu <- matrix(1:9, nrow = 3, ncol = 3)
matriu
apply(matriu, 2, sum)

```

---

## future.apply::future_apply()

Objectiu: Proporcionar **alternatives de treball en paral·lel** sense preocupacions a les funcions `apply` de l'R base. Exemple, en comptes de fer:

```{r eval=F}
p_load("stats")
x <- 1:10
y <- lapply(x, FUN = quantile, probs = 1:3/4)
```

es pot fer:
```{r eval=F}
p_load("future.apply")
plan(multiprocess) ## Run in parallel on local computer

x <- 1:10
y <- future_lapply(x, FUN = quantile, probs = 1:3/4)
```

De forma anàloga, el paquet `future` va en la línia de permetre paral·lelitzar qualsevol funció de `R base`.

---

## Exemple breu amb dataset b - SAIER
### Carreguem les dades  

```{r message=FALSE}
arxiu <- read_csv("../dades/b/saier_2017.csv")
arxiu <- clean_names(arxiu); head(arxiu)
```

---

### Exemple amb bucle i crida manual

```{r}
resultats <- list()
tic("Exemple sobre saier_2017.csv amb bucle")
for (n in 1:length(colnames(arxiu))) {
  resultats[[n]] <- summary(arxiu[,n])
}
resultats[1:2] # es mostren els primers 2 elements només ara a tal demostratiu
toc()
```


---

### Exemple amb `apply()`

```{r}
tic("Exemple sobre saier_2017.csv amb apply()")
resultats <- apply(arxiu, 2, summary)
#resultats <- sapply(arxiu, summary) # versió equivalent de apply simplificat
resultats[,1:3] # es mostren els primers 2 elements només ara a tal demostratiu
toc()
```


---

### Exemple amb `map()`

Aplica la funció sobre un objecte d'R, i retorna una llista de resultats.

```{r}
tic("Exemple sobre saier_2017.csv amb map()")
resultats <- purrr::map(arxiu, summary)
resultats[1:2] # es mostren els primers 2 elements només ara a tal demostratiu
toc()
```

---

### Exemple amb `future.apply()` (paral.lelització){.smaller}

Executant la feina seqüencialment amb un sol processador (core):

```{r}
plan(sequential)
tic("Exemple sobre saier_2017.csv amb future.apply() SEQÜENCIAL")
resultats <- future.apply::future_apply(arxiu, 2, max)
toc()
```

Ara llencem la mateixa feina, dividint la feina entre els diferents nuclis de l'ordinador (**paral.lelització**):

```{r}
plan(multiprocess) 
# Plan: sequential | multisession | multicore (no windows) | multiprocess | cluster | remote
# veure ?plan
tic("Exemple sobre saier_2017.csv amb future.apply()")
resultats <- future.apply::future_apply(arxiu, 2, max)
toc()
```

**Compte amb paral·lelitzar feines petites!**: es triga més en dividir la feina entre els differents nuclis de l'ordinador o cluster que no pas llencar tota la feina (que no sigui massa gran) a un sol nucli. 

---

### Exemple amb `future_map()`{.smaller}

Aplica la funció sobre un objecte d'R, i retorna una llista de resultats.

```{r}
plan(multiprocess) 
# Plan: sequential | multisession | multicore (no windows) | multiprocess | cluster | remote
# veure ?plan
tic("Exemple sobre saier_2017.csv amb future_map()")
resultats <- furrr::future_map(arxiu, summary)

toc()
```

**Compte amb paral·lelitzar feines petites!**: es triga més en dividir la feina entre els differents nuclis de l'ordinador o cluster que no pas llencar tota la feina (que no sigui massa gran) a un sol nucli. 

---

## Exemple breu amb dataseet d: validació dnis{.smaller}

Podem aplicar una funció de comprovar si la lletra d'un dni/nif és correcta. Exemple: `check.nif.letter()`. O una altra funció per indicar quina seria la lletra correcta: `fix.nif.letter()` (ambdues del paquet `omd.tractaments`, al [gitlab de l'Ajuntament de Barcelona](gitlab.ajuntament.bcn/omd-gid/) ).

Podem aplicar aquestes funcions sobre vectors llargs de dni a través de les funcions de la família dels `apply()` o `future.apply()`.

Ho provarem sobre el conjunt de dades `d` de **validació dels dni i nie**

```{r message=FALSE}
# Carreguem les dades i netegem els noms de les columnes
signants <- readxl::read_xls("../dades/d/signants.xls", sheet="Signants", skip = 1)
signants <- clean_names(signants); signants[1:4,]
```

---

### Carreguem a la sessió funcions de comprovar dnis 

I comprovem lletres de dni sobre elements individuals: 

```{r}
source("funcio.check.nif.letter.R")
# La funció check.nif.letter() espera un dni com a argument, i retorna TRUE/FALSE: TRUE si és correcta la lletra, i FALSE si és invàlida i cal corregir-la.
check.nif.letter("1234567A")
```

Quan el resultat és FALSE, cal corregir la lletra.

```{r}
source("funcio.fix.nif.letter.R")
fix.nif.letter("1234567A")
```

---

### Comprovem lletres de dni sobre vectors 

```{r}
check.nif.letter(signants$dni_i_o_nie)
```

Quan el resultat és FALSE, cal corregir la lletra.

```{r}
fix.nif.letter(signants$dni_i_o_nie)
```

---

### Exemple amb `sapply()`

Versió amigable de l'apply retornant un vector o matriu habitualment.
```{r warning=FALSE}
tic("Exemple sobre signants.xls amb sapply()")
sapply(signants$dni_i_o_nie, check.nif.letter)
toc()
```



---

### Afegeix una columna extra a signants amb nif amb lletra corregida
Recorda que pots emprar la funció `fix.nif.letter()` (carregada abans en memòria amb un `source()` de l'arxiu .R que la conté), de forma anàloga a com s'ha mostrat per a la funció `check.nif.letter()`.

I que per afegir una columna sobre un dataframe previ pots emprar la funció `dplyr::mutate()`


```{r}
# Escriu el codi necessari en aquest chunk d'R:
#
```



---

## Més informació

Per continuar practicant o aprendre més:

* https://github.com/rstudio/webinars/blob/master/01-Grammar-and-Graphics-of-Data-Science/1-dplyr-tidyr.pdf
* https://github.com/rstudio/webinars/blob/master/05-Data-Wrangling-with-R-and-RStudio/wrangling-webinar.pdf
* https://cran.r-project.org/web/views/HighPerformanceComputing.html


---

## Agraïments

* Companys del Departament de Gestió i Integració de dades de la OMD, AjBCN
* Equip de [Rstudio](https://www.rstudio.com/)

---

# Exercici
* Exercici **exercici.06.Rmd** (o _exercici.06.pdf_)

