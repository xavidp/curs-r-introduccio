---
title: "Curs d`R"
subtitle: "Sessió 11: Sessió extra."
institute: 'Oficina Municipal de Dades (OMD). Ajuntament de Barcelona'
author: Xavier de Pedro Puente  (OMD-AjBCN)
date: "Dijous 23 de Maig de 2019 (generat el `r format(Sys.time(), '%d %B %Y')`)"
output: 
  slidy_presentation:
    footer: "Copyleft 2019, OMD-AjBCN. http://omd-gid.imi.bcn/CursR"
    number_sections: TRUE
    includes:
      in_header: apunts_capcalera.html
      after_body: apunts_peu.html
    css: styles.css 
always_allow_html: yes
tags:
- Canvis
- Git
- DVCS
- Gitlab
- Control
- Diff
- Merge
- R
- Tidyverse
abstract: |
  Més informació a:
  https://gitlab.com/radup/curs-r-introduccio`
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message =FALSE)

# Set overall timer to zero
start_all <- Sys.time()
```

---

# Sessió Extra (Dj. 23/05/19){.smaller}

Treballar amb un conjunt de dades nou: o vostre de la feina, o de l'OpenData de l'Ajuntament de Barcelona https://opendata-ajuntament.barcelona.cat

Tasques a fer durant la sessió:

1. Crear un **informe en R Markdown**
1. Treballar en Rstudio amb **control de canvis via git**, contra un projecte de gitlab (el de l'Ajuntament de Barcelona si és per dades vostres, o a gitlab.com si és d'OpenData)
1. Per a l'informe caldrà:
    1. **Llegir l'arxiu** (o arxius) de dades a la sessió d'R, 
    1. Fer com a mínim una **taula**
    1. Fer com a mínim un **gràfic**
    1. Fer com a mínim un **mapa**
    1. **Desar l'informe de resultats** en disc, en html o pdf, i les dades processades en csv.
    1. **Afegir carpeta** amb els arxius del vostre informe **al repositori del gitlab corresponent**
        * Si són dades obertes, a  https://gitlab.com/radup/curs-r-introduccio-demo-git
        * Si són dades NO obertes, de la vostre feina, a repo dins https://gitlab.ajuntament.bcn o altres.
