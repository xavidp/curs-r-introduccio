# Curs R Introducció

https://gitlab.com/radup/curs-r-introduccio

Curs d'Introducció al programari R per a anàlisi de dades, elaboració de gràfics i mapes, informes dinàmics. 

Els assistents al curs, en finalitzar-lo, s'espera que hagin après:
1. A moure's per l'entorn del programa RStudio
1. Què són els objectes en R i com treballar amb ells (crear, esborrar, operar)
1. Què és pot fer amb R - 1a part (core: funcions bàsiques, llibreries, ...)
1. Importar dades de forma fàcil (csv vs csv2, encodings, ...)
1. Analitzar dades, tant numèriques com dates o cadenes de text (comparativa sintaxi _R base_ vs _TidyVerse_), i fer estadística bàsica
1. Què és pot fer amb R - 2a part (core: grep, gsub, regexp, *apply (evitar bucles amb for sempre que es pugui)
1. Què és pot fer amb R - 3a part (endinsar-se en el _TidyVerse_ : codi llegible, encadenat amb pipes, vectoritzat "de franc", ...)
1. Fer Taules
1. Fer Gràfics
1. Fer Mapes (amb tesel·les i/o arxius .shp, estàtics i/o interactius)
1. Outputs amb R
1. Gestió de Colors (RGB, conversió entre formats, emprar ColorBrewer i similars, tenir en compte daltonisme)
1. Coneixer què són els htmlwidgets i alguns dels més utils
1. Control de Canvis i Compartir codi

---

Copyright - Copyleft'ed with a Dual License:

(1) MIT License for software
    https://choosealicense.com/licenses/mit/

(2) Attribution-ShareAlike 4.0 International - for documentation 
    https://choosealicense.com/licenses/cc-by-sa-4.0/